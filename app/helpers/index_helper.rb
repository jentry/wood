module IndexHelper
  def item_link(item)
    return "/catalog/#{item.category.slug}/#{item.slug}"
  end
end
