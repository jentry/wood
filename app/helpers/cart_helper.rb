module CartHelper

  def countcart
    @cart = session[:cart]
    @count = 0
    if @cart
      @cart.each do |i,v|
        @count = @count + v.to_i
      end
    end

    @count  
  end

end
