$(document).on('page:change', function(){
  var token = $( 'meta[name="csrf-token"]' ).attr( 'content' );
  $('a.addtocart').on('click', function(e){
    e.preventDefault();
    var _id = $(this).attr('data-id');
    var _count = $(this).attr('data-count');
    
    $.ajax({
      type: "POST",
      url : "/addtocart",
      dataType: 'json',
      beforeSend: function ( xhr ) {
        xhr.setRequestHeader( 'X-CSRF-Token', token );
      },
      data: {id : _id, count: _count},
      error: function(data){
        console.log(data);
      },
      success: function(data){
        $('.small_cart span').text(data.count);
      }
    });
  });

  if($('html').hasClass('desktop')){
    $.stellar({
      horizontalOffset: 40,
      verticalOffset: 0
    });
  }

  $('#first_screen nav ul li a').on('click', function(e){
    e.preventDefault();
    var _block = $($(this).attr('href')).offset().top - 90;
    $('html, body').animate({scrollTop : _block}, 1000);
  });
});