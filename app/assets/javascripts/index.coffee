# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready( ->
  $('.bxslider').bxSlider({
    minSlides: 1,
    maxSlides: 3,
    pager: false,
    auto: true,
    slideWidth: 300,
    slideMargin: 30,
    pause: 8000
  })
          
)