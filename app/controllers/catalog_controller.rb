class CatalogController < ApplicationController
  def index
    @cats = Admin::Category.all
  end

  def category
    @cat = Admin::Category.find_by_slug(params[:category])
  end

  def item
    @item = Admin::Item.find_by_slug(params[:item])
  end
end
