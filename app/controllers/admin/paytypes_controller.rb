class Admin::PaytypesController < ApplicationController
  layout "admin"
  before_filter :custom_user_auth
  before_action :set_admin_paytype, only: [:show, :edit, :update, :destroy]

  # GET /admin/paytypes
  # GET /admin/paytypes.json
  def index
    @admin_paytypes = Admin::Paytype.all
  end

  # GET /admin/paytypes/1
  # GET /admin/paytypes/1.json
  def show
  end

  # GET /admin/paytypes/new
  def new
    @admin_paytype = Admin::Paytype.new
  end

  # GET /admin/paytypes/1/edit
  def edit
  end

  # POST /admin/paytypes
  # POST /admin/paytypes.json
  def create
    @admin_paytype = Admin::Paytype.new(admin_paytype_params)

    respond_to do |format|
      if @admin_paytype.save
        format.html { redirect_to @admin_paytype, notice: 'Paytype was successfully created.' }
        format.json { render :show, status: :created, location: @admin_paytype }
      else
        format.html { render :new }
        format.json { render json: @admin_paytype.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/paytypes/1
  # PATCH/PUT /admin/paytypes/1.json
  def update
    respond_to do |format|
      if @admin_paytype.update(admin_paytype_params)
        format.html { redirect_to @admin_paytype, notice: 'Paytype was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_paytype }
      else
        format.html { render :edit }
        format.json { render json: @admin_paytype.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/paytypes/1
  # DELETE /admin/paytypes/1.json
  def destroy
    @admin_paytype.destroy
    respond_to do |format|
      format.html { redirect_to admin_paytypes_url, notice: 'Paytype was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_paytype
      @admin_paytype = Admin::Paytype.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_paytype_params
      params.require(:admin_paytype).permit(:title)
    end
end
