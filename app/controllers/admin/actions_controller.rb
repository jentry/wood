class Admin::ActionsController < ApplicationController
  layout "admin"
  before_filter :custom_user_auth
  before_action :set_admin_action, only: [:show, :edit, :update, :destroy]

  # GET /admin/actions
  # GET /admin/actions.json
  def index
    @admin_actions = Admin::Action.all
  end

  # GET /admin/actions/1
  # GET /admin/actions/1.json
  def show
  end

  # GET /admin/actions/new
  def new
    @admin_action = Admin::Action.new
  end

  # GET /admin/actions/1/edit
  def edit
  end

  # POST /admin/actions
  # POST /admin/actions.json
  def create
    @admin_action = Admin::Action.new(admin_action_params)

    respond_to do |format|
      if @admin_action.save
        format.html { redirect_to @admin_action, notice: 'Action was successfully created.' }
        format.json { render :show, status: :created, location: @admin_action }
      else
        format.html { render :new }
        format.json { render json: @admin_action.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/actions/1
  # PATCH/PUT /admin/actions/1.json
  def update
    respond_to do |format|
      if @admin_action.update(admin_action_params)
        format.html { redirect_to @admin_action, notice: 'Action was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_action }
      else
        format.html { render :edit }
        format.json { render json: @admin_action.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/actions/1
  # DELETE /admin/actions/1.json
  def destroy
    @admin_action.destroy
    respond_to do |format|
      format.html { redirect_to admin_actions_url, notice: 'Action was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_action
      @admin_action = Admin::Action.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_action_params
      params.require(:admin_action).permit(:title, :text, :image)
    end
end
