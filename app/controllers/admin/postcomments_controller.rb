class Admin::PostcommentsController < ApplicationController
  layout "admin"
  before_filter :custom_user_auth
  before_action :set_admin_postcomment, only: [:show, :edit, :update, :destroy]

  # GET /admin/postcomments
  # GET /admin/postcomments.json
  def index
    @admin_postcomments = Admin::Postcomment.all
  end

  # GET /admin/postcomments/1
  # GET /admin/postcomments/1.json
  def show
  end

  # GET /admin/postcomments/new
  def new
    @admin_postcomment = Admin::Postcomment.new
  end

  # GET /admin/postcomments/1/edit
  def edit
  end

  # POST /admin/postcomments
  # POST /admin/postcomments.json
  def create
    @admin_postcomment = Admin::Postcomment.new(admin_postcomment_params)

    respond_to do |format|
      if @admin_postcomment.save
        format.html { redirect_to @admin_postcomment, notice: 'Postcomment was successfully created.' }
        format.json { render :show, status: :created, location: @admin_postcomment }
      else
        format.html { render :new }
        format.json { render json: @admin_postcomment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/postcomments/1
  # PATCH/PUT /admin/postcomments/1.json
  def update
    respond_to do |format|
      if @admin_postcomment.update(admin_postcomment_params)
        format.html { redirect_to @admin_postcomment, notice: 'Postcomment was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_postcomment }
      else
        format.html { render :edit }
        format.json { render json: @admin_postcomment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/postcomments/1
  # DELETE /admin/postcomments/1.json
  def destroy
    @admin_postcomment.destroy
    respond_to do |format|
      format.html { redirect_to admin_postcomments_url, notice: 'Postcomment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_postcomment
      @admin_postcomment = Admin::Postcomment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_postcomment_params
      params.require(:admin_postcomment).permit(:author, :email, :text, :post_id)
    end
end
