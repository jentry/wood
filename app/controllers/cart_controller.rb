class CartController < ApplicationController

  def add
    
    @cart = session[:cart]

    unless @cart
      @cart = {}
      @cart[params[:id]] = params[:count]
    else
      if @cart[params[:id]]
        new_value = @cart[params[:id]].to_i + params[:count].to_i
        @cart[params[:id]] = new_value
      else
        @cart[params[:id]] = params[:count]
      end
    end

    session[:cart] = @cart
    
    @count = 0
    
    @cart.each do |i,v|
      @count = @count + v.to_i
    end
    
    respond_to do |format|
      msg = { :count => @count }
      format.json  { render :json => msg }
    end
  end

  def index
    cart_items()
  end

  def checkout
    cart_items()
  end

  def order
    #cart_items()
    ids = Array.new
    @cart = session[:cart]
    if @cart
      @cart.each do |k,v|
          ids.push(k)
        end
    end

    order = Admin::Order.new
    order.name = params[:name]
    order.email = params[:email]
    order.summ = params[:summa]
    order.paytype_id = params[:paytype]
    order.payed = false
    order.phone = params[:phone]
    order.comment = params[:comment]
    order.item_ids = ids
    if order.save
      session[:cart] = nil
      redirect_to 'cart/success'
    else
      redirect_to 'cart/checkout'
    end  

  end

  def success

  end

  private 
    def cart_items
      ids = Array.new
      @total_summ = 0
      @cart_count = 0
      @cart = session[:cart]
      
      if @cart
        @cart.each do |k,v|
          ids.push(k)
        end
        @cart_items = Admin::Item.where(id: ids)
        @cart_count = @cart.length
        @cart_items.each do |item|
          @total_summ = @total_summ + (item.price.to_i * @cart[item.id.to_s].to_i)
        end
      end
    end
end
