class Admin::Item < ActiveRecord::Base
  extend FriendlyId
  belongs_to :category, :class_name => "Category"
  belongs_to :action
  has_many :comments, :class_name => "Comment", :foreign_key => "item_id", :dependent => :destroy
  has_and_belongs_to_many :orders
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

 
  friendly_id :title, use: :slugged

  def normalize_friendly_id(text)
    text.to_slug.normalize(transliterations: :russian).to_s
  end
end
