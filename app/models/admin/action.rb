class Admin::Action < ActiveRecord::Base
  extend FriendlyId
  has_many :items, :class_name => "Item", :foreign_key => "action_id", :dependent => :nullify
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  friendly_id :title, use: :slugged

  def normalize_friendly_id(text)
    text.to_slug.normalize(transliterations: :russian).to_s
  end
end
