class Admin::Paytype < ActiveRecord::Base
  has_many :orders, :class_name => "Order", :foreign_key => "paytype_id", :dependent => :destroy
end
