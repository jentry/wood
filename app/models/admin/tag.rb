class Admin::Tag < ActiveRecord::Base
  has_and_belongs_to_many :posts
  extend FriendlyId
  friendly_id :title, use: :slugged

   def normalize_friendly_id(text)
    text.to_slug.normalize(transliterations: :russian).to_s
  end
end
