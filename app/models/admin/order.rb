class Admin::Order < ActiveRecord::Base
  belongs_to :paytype, :class_name => "Paytype"
  has_and_belongs_to_many :items
end
