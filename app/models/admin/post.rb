class Admin::Post < ActiveRecord::Base
  extend FriendlyId
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  has_and_belongs_to_many :tags
  friendly_id :title, use: :slugged
  has_many :postcomments, :class_name => "Postcomment", :foreign_key => "post_id", :dependent => :destroy

  def normalize_friendly_id(text)
    text.to_slug.normalize(transliterations: :russian).to_s
  end
end
