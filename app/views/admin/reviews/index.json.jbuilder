json.array!(@admin_reviews) do |admin_review|
  json.extract! admin_review, :id, :name, :city, :text, :foto
  json.url admin_review_url(admin_review, format: :json)
end
