json.array!(@admin_paytypes) do |admin_paytype|
  json.extract! admin_paytype, :id, :title
  json.url admin_paytype_url(admin_paytype, format: :json)
end
