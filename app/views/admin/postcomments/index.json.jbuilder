json.array!(@admin_postcomments) do |admin_postcomment|
  json.extract! admin_postcomment, :id, :author, :email, :text, :post_id_id
  json.url admin_postcomment_url(admin_postcomment, format: :json)
end
