json.array!(@admin_items) do |admin_item|
  json.extract! admin_item, :id, :title, :price, :description, :sku, :image, :category, :rating, :price_old, :quantity, :seotitle, :seodesc, :seokeywords, :actionitem
  json.url admin_item_url(admin_item, format: :json)
end
