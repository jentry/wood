json.array!(@admin_comments) do |admin_comment|
  json.extract! admin_comment, :id, :name, :email, :text, :rating
  json.url admin_comment_url(admin_comment, format: :json)
end
