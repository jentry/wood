json.array!(@admin_actions) do |admin_action|
  json.extract! admin_action, :id, :title, :text, :image
  json.url admin_action_url(admin_action, format: :json)
end
