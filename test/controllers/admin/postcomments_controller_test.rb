require 'test_helper'

class Admin::PostcommentsControllerTest < ActionController::TestCase
  setup do
    @admin_postcomment = admin_postcomments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_postcomments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_postcomment" do
    assert_difference('Admin::Postcomment.count') do
      post :create, admin_postcomment: { author: @admin_postcomment.author, email: @admin_postcomment.email, post_id_id: @admin_postcomment.post_id_id, text: @admin_postcomment.text }
    end

    assert_redirected_to admin_postcomment_path(assigns(:admin_postcomment))
  end

  test "should show admin_postcomment" do
    get :show, id: @admin_postcomment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_postcomment
    assert_response :success
  end

  test "should update admin_postcomment" do
    patch :update, id: @admin_postcomment, admin_postcomment: { author: @admin_postcomment.author, email: @admin_postcomment.email, post_id_id: @admin_postcomment.post_id_id, text: @admin_postcomment.text }
    assert_redirected_to admin_postcomment_path(assigns(:admin_postcomment))
  end

  test "should destroy admin_postcomment" do
    assert_difference('Admin::Postcomment.count', -1) do
      delete :destroy, id: @admin_postcomment
    end

    assert_redirected_to admin_postcomments_path
  end
end
