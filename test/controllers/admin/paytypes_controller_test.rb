require 'test_helper'

class Admin::PaytypesControllerTest < ActionController::TestCase
  setup do
    @admin_paytype = admin_paytypes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_paytypes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_paytype" do
    assert_difference('Admin::Paytype.count') do
      post :create, admin_paytype: { title: @admin_paytype.title }
    end

    assert_redirected_to admin_paytype_path(assigns(:admin_paytype))
  end

  test "should show admin_paytype" do
    get :show, id: @admin_paytype
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_paytype
    assert_response :success
  end

  test "should update admin_paytype" do
    patch :update, id: @admin_paytype, admin_paytype: { title: @admin_paytype.title }
    assert_redirected_to admin_paytype_path(assigns(:admin_paytype))
  end

  test "should destroy admin_paytype" do
    assert_difference('Admin::Paytype.count', -1) do
      delete :destroy, id: @admin_paytype
    end

    assert_redirected_to admin_paytypes_path
  end
end
