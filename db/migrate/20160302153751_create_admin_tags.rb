class CreateAdminTags < ActiveRecord::Migration
  def change
    create_table :admin_tags do |t|
      t.string :title
      t.string :slug, unique: true, index: true

      t.timestamps null: false
    end
  end
end
