class CreateAdminPosts < ActiveRecord::Migration
  def change
    create_table :admin_posts do |t|
      t.string :title
      t.string :slug, unique: true, index: true
      t.text :content
      t.attachment :image

      t.timestamps null: false
    end
  end
end
