class AddSlugtoPost < ActiveRecord::Migration
  def change
    add_column :admin_posts, :slug, :string
    add_index :admin_posts, :slug, unique: true
  end
end
