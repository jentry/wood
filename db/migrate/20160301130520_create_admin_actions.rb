class CreateAdminActions < ActiveRecord::Migration
  def change
    create_table :admin_actions do |t|
      t.string :title
      t.text :text
      t.attachment :image

      t.timestamps null: false
    end
  end
end
