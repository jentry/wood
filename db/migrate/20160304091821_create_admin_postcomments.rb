class CreateAdminPostcomments < ActiveRecord::Migration
  def change
    create_table :admin_postcomments do |t|
      t.string :author
      t.string :email
      t.text :text
      t.references :post_id, index: true

      t.timestamps null: false
    end
  end
end
