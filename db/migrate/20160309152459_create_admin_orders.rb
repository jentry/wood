class CreateAdminOrders < ActiveRecord::Migration
  def change
    create_table :admin_orders do |t|
      t.string :name
      t.string :email
      t.integer :summ
      t.boolean :payed
      t.references :paytype_id, index: true
      t.string :phone
      t.text :comment

      t.timestamps null: false
    end
  end
end
