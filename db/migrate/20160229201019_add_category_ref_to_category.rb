class AddCategoryRefToCategory < ActiveRecord::Migration
  def change
    add_reference :admin_categories, :parent, index: true
  end
end
