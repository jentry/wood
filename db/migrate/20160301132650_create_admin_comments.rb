class CreateAdminComments < ActiveRecord::Migration
  def change
    create_table :admin_comments do |t|
      t.string :name
      t.string :email
      t.text :text
      t.integer :rating
      t.references :item, index: true

      t.timestamps null: false
    end
  end
end
