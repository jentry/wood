class CreateAdminPaytypes < ActiveRecord::Migration
  def change
    create_table :admin_paytypes do |t|
      t.string :title

      t.timestamps null: false
    end
  end
end
