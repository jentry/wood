class AddSlugColumnToAdminPages < ActiveRecord::Migration
  def change
    add_column :admin_pages, :slug, :string
    add_index :admin_pages, :slug, unique: true
  end
end
