class AddColumnToActionsSlug < ActiveRecord::Migration
  def change
    add_column :admin_actions, :slug, :string
    add_index :admin_actions, :slug, unique: true
  end
end
