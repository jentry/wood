class AddSlugToItems < ActiveRecord::Migration
  def change
    add_column :admin_items, :slug, :string
    add_index :admin_items, :slug, unique: true
  end
end
