class CreateAdminCategories < ActiveRecord::Migration
  def change
    create_table :admin_categories do |t|
      t.string :title
      t.string :seotitle
      t.text :seodesc
      t.string :seokeywords
      t.text :description
      t.attachment :image

      t.timestamps null: false
    end
  end
end
