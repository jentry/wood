class AddActionToItems < ActiveRecord::Migration
  def change
    add_reference :admin_items, :action, index: true
  end
end
