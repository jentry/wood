class CreateJoinTableAdminpostAdmintag < ActiveRecord::Migration
  def change
    create_join_table :admin_posts, :admin_tags do |t|
       t.index [:admin_post_id, :admin_tag_id]
       t.index [:admin_tag_id, :admin_post_id]
    end
  end
end
