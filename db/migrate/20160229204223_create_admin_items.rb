class CreateAdminItems < ActiveRecord::Migration
  def change
    create_table :admin_items do |t|
      t.string :title
      t.integer :price
      t.text :description
      t.string :sku
      t.attachment :image
      t.references :category, index: true
      t.integer :rating
      t.integer :price_old
      t.integer :quantity
      t.string :seotitle
      t.string :seodesc
      t.string :seokeywords
      t.boolean :actionitem

      t.timestamps null: false
    end
  end
end
