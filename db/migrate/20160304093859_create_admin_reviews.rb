class CreateAdminReviews < ActiveRecord::Migration
  def change
    create_table :admin_reviews do |t|
      t.string :name
      t.string :city
      t.text :text
      t.attachment :foto

      t.timestamps null: false
    end
  end
end
