# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160403142656) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_actions", force: :cascade do |t|
    t.string   "title"
    t.text     "text"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "slug"
  end

  add_index "admin_actions", ["slug"], name: "index_admin_actions_on_slug", unique: true, using: :btree

  create_table "admin_categories", force: :cascade do |t|
    t.string   "title"
    t.string   "seotitle"
    t.text     "seodesc"
    t.string   "seokeywords"
    t.text     "description"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "parent_id"
    t.string   "slug"
  end

  add_index "admin_categories", ["parent_id"], name: "index_admin_categories_on_parent_id", using: :btree
  add_index "admin_categories", ["slug"], name: "index_admin_categories_on_slug", unique: true, using: :btree

  create_table "admin_comments", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.text     "text"
    t.integer  "rating"
    t.integer  "item_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "admin_comments", ["item_id"], name: "index_admin_comments_on_item_id", using: :btree

  create_table "admin_items", force: :cascade do |t|
    t.string   "title"
    t.integer  "price"
    t.text     "description"
    t.string   "sku"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "category_id"
    t.integer  "rating"
    t.integer  "price_old"
    t.integer  "quantity"
    t.string   "seotitle"
    t.string   "seodesc"
    t.string   "seokeywords"
    t.boolean  "actionitem"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "action_id"
    t.string   "slug"
  end

  add_index "admin_items", ["action_id"], name: "index_admin_items_on_action_id", using: :btree
  add_index "admin_items", ["category_id"], name: "index_admin_items_on_category_id", using: :btree
  add_index "admin_items", ["slug"], name: "index_admin_items_on_slug", unique: true, using: :btree

  create_table "admin_items_orders", id: false, force: :cascade do |t|
    t.integer "order_id", null: false
    t.integer "item_id",  null: false
  end

  add_index "admin_items_orders", ["item_id", "order_id"], name: "index_items_orders_on_item_id_and_order_id", using: :btree
  add_index "admin_items_orders", ["order_id", "item_id"], name: "index_items_orders_on_order_id_and_item_id", using: :btree

  create_table "admin_orders", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.integer  "summ"
    t.boolean  "payed"
    t.integer  "paytype_id"
    t.string   "phone"
    t.text     "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "admin_orders", ["paytype_id"], name: "index_admin_orders_on_paytype_id_id", using: :btree

  create_table "admin_pages", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "slug"
  end

  add_index "admin_pages", ["slug"], name: "index_admin_pages_on_slug", unique: true, using: :btree

  create_table "admin_paytypes", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "admin_postcomments", force: :cascade do |t|
    t.string   "author"
    t.string   "email"
    t.text     "text"
    t.integer  "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "admin_postcomments", ["post_id"], name: "index_admin_postcomments_on_post_id_id", using: :btree

  create_table "admin_posts", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "slug"
  end

  add_index "admin_posts", ["slug"], name: "index_admin_posts_on_slug", unique: true, using: :btree

  create_table "admin_posts_tags", id: false, force: :cascade do |t|
    t.integer "post_id", null: false
    t.integer "tag_id",  null: false
  end

  add_index "admin_posts_tags", ["post_id", "tag_id"], name: "index_admin_posts_tags_on_admin_post_id_and_admin_tag_id", using: :btree
  add_index "admin_posts_tags", ["tag_id", "post_id"], name: "index_admin_posts_tags_on_admin_tag_id_and_admin_post_id", using: :btree

  create_table "admin_reviews", force: :cascade do |t|
    t.string   "name"
    t.string   "city"
    t.text     "text"
    t.string   "foto_file_name"
    t.string   "foto_content_type"
    t.integer  "foto_file_size"
    t.datetime "foto_updated_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "admin_tags", force: :cascade do |t|
    t.string   "title"
    t.string   "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "admin_tags", ["slug"], name: "index_admin_tags_on_slug", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "identities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "name"
    t.string   "city"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  add_foreign_key "identities", "users"
end
